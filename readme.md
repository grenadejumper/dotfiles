Repo containing dotfiles for various Arch linux configs I use.

Most of the dotfiles are located in the home directory, so just symlink them like this:

	ln -s /home/username/dotfiles/vimrc /home/username/.vimrc

More info on dotfiles here: http://blog.smalleycreative.com/tutorials/using-git-and-github-to-manage-your-dotfiles/
